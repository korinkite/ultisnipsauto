# A generic file


class BaseDocstringsTestCase(object):

    '''Sets up the main docstrings to be used in other test cases.'''

    docstring = \
    """

    def some_func():
        pass
    # end some_func

    def the_actual_function(someArg, anotherArg1=[], lastArg={}):
        '''This is a test docstring to test for.

        This is some additional information about the docstring

        Args:
            someArg (something): Description here
            anotherArg1 (list): Another description that spans multiple
                                lines and is needlessly long
            lastArg (set): Some last description

        Returns:
            bool : The return type description of the current function

        '''
        return True
    # end the_actual_function

    """
    singleLineDocstring = \
    """
    def some_func():
        pass
    # end some_func

    def the_actual_function():
        '''This is a test docstring to test for.'''
        pass
    # end the_actual_function

    """

    def __init__(self, *args, **kwargs):
        '''Creates a consistent docstring across all tests.'''
        super(BaseDocstringsTestCase, self).__init__(*args, **kwargs)
    # end __init__
# end BaseDocstringsTestCase


if __name__ == '__main__':
    print(__doc__)

