#!/usr/bin/env python
# -*- coding: utf-8 -*-

# IMPORT STANDARD LIBRARIES
import os
import unittest

# IMPORT LOCAL LIBRARIES
from pysix import six
from pythonx import helper
import common


class helper(object):

    '''dummy class.'''

    def __init__(self):
        '''.'''
        super(helper, self).__init__()
    # end __init__

    @classmethod
    def get_docstring_bounds(cls, vimLines, rowIndex=None, offsetBounds=0,
                         splitBy=os.linesep):
        '''Given the current line, search for and return the wrapped docstring.

        Args:
            rowIndex (int): The current line

        '''
        startOfDocstring = 0
        endOfDocstring = -1

        if isinstance(vimLines, six.string_types):
            vimLines = vimLines.split('\n')

        if rowIndex is None:
            # CJK : Get the current line, somehow
            (rowIndex, _) = vim.current.window.cursor
            rowIndex -= 1

        for i, line in enumerate(vimLines[rowIndex:]):
            if "'''" in line or '"""' in line:
                endOfDocstring = i + rowIndex + 1

        for i, line in enumerate(reversed(vimLines[:rowIndex])):
            if "'''" in line or '"""' in line:
                startOfDocstring = rowIndex - i

        if startOfDocstring - offsetBounds >= 0:
            startOfDocstring -= offsetBounds

        if endOfDocstring + offsetBounds <= len(vimLines):
            endOfDocstring += offsetBounds

        return (startOfDocstring - offsetBounds, endOfDocstring)
    # end get_docstring_bounds
# end helper


class DocstringGrabTestCase(common.BaseDocstringsTestCase, unittest.TestCase):

    '''Verifies methods for grabbing docstrings in different contexts.'''

    def __init__(self, *args, **kwargs):
        super(DocstringGrabTestCase, self).__init__(*args, **kwargs)
    # end __init__

    def _test_docstring(self, docstring, cursorPos, output, offsetBounds=0):
        '''.

        Args:
            docstring (str or iter)
            offsetBound (int)

        '''
        start, end = helper.get_docstring_bounds(docstring, cursorPos,
                                                 offsetBounds=offsetBounds)
        if isinstance(docstring, six.string_types):
            docstring = docstring.split('\n')

        # self.assertEqual(('\n').join(docstring[start:end]), output)
    # end _test_docstring

    def setUp(self):
        '''Adds the outputs of the tests in the current class.'''
        super(common.BaseDocstringsTestCase, self).setUp()

        # Init some docstring function results
        self.justTheFuncDoc = \
        """\
        '''This is a test docstring to test for.

        This is some additional information about the docstring

        Args:
            someArg (something): Description here
            anotherArg1 (list): Another description that spans multiple
                                lines and is needlessly long
            lastArg (set): Some last description

        Returns:
            bool : The return type description of the current function

        '''"""

        self.justTheFuncDocSingle = "'''This is a test docstring to test for.'''"

        self.offsetBoundsDocstring3 = \
        """\
        # end some_func

        def the_actual_function(someArg, anotherArg1=[], lastArg={}):
            '''This is a test docstring to test for.

            This is some additional information about the docstring

            Args:
                someArg (something): Description here
                anotherArg1 (list): Another description that spans multiple
                                    lines and is needlessly long
                lastArg (set): Some last description

            Returns:
                bool : The return type description of the current function

            '''
            return True
        # end the_actual_function

        """

        self.offsetBoundsDocstring2 = self.offsetBoundsDocstring3[1:-2]
        self.offsetBoundsDocstring1 = self.offsetBoundsDocstring2[1:-2]
        self.midCursorPos = 10  # somewhere in the middle of the docstring
        self.midCursorPosSingle = 6  # The line of the single-line docstring
    # end setUp

    def test_get_docstring(self):
        '''Gets the docstring in the func, given the cursor position.'''
        self._test_docstring(self.docstring, self.midCursorPos,
                             self.justTheFuncDoc)
    # end test_get_docstring

    def test_get_docstring_from_list(self):
        '''Gets docstring, given a list of lines from a buffer.'''
        self._test_docstring(self.docstring.split(os.linesep),
                             self.midCursorPos, self.justTheFuncDoc)
    # end test_get_docstring_from_list

    def test_get_docstring_offsetBounds1(self):
        '''Gets the docstring with offsetBounds, given the cursor position.'''
        self._test_docstring(self.docstring, self.midCursorPos,
                             self.offsetBoundsDocstring1, 1)
    # end test_get_docstring_offsetBounds1

    def test_get_docstring_offsetBounds2(self):
        '''Gets the docstring with offsetBounds, given the cursor position.'''
        self._test_docstring(self.docstring, self.midCursorPos,
                             self.offsetBoundsDocstring2, 2)
     # end test_get_docstring_offsetBounds2

    def test_get_docstring_offsetBounds3(self):
        '''Gets the docstring with offsetBounds, given the cursor position.'''
        self._test_docstring(self.docstring, self.midCursorPos,
                             self.offsetBoundsDocstring3, 3)
    # end test_get_docstring_offsetBounds3

    def test_get_single_line_docstring(self):
        '''Gets a docstring that is only 1 line long.'''
        self._test_docstring(self.docstring, self.midCursorPos,
                             self.justTheFuncDocSingle, 3)
    # end test_get_single_line_docstring
# end DocstringGrabTestCase

if __name__ == '__main__':
    unittest.main()

