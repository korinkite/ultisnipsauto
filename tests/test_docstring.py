#!/usr/bin/env python
# -*- coding: utf-8 -*-

# IMPORT LOCAL LIBRARIES
import docstring
import common


class DocAnalysisTestCase(common.BaseDocstringsTestCase, unittest.TestCase):

    '''Inspects and gets information about a full docstring.'''

    def __init__(self, *args, **kwargs):
        super(DocstringAnalysisTestCase, self).__init__(*args, **kwargs)
    # end __init__

    def test_get_docstring_with_ast(self):
        '''Uses the ast module to get a doctsring from a snippet of code.'''
        docstring.crop_to_docstring(self.docstring)
    # end test_get_docstring_with_ast
# end DocAnalysisTestCase


# if __name__ == '__main__':
#     unittest.main()


