if exists('did_plugin_ultisnips_automate') || &cp
    finish
endif
let did_plugin_ultisnips_automate=1

" Init variables
if !exists('g:ua_temp_folder')
	let g:ua_temp_folder = 'C:/Temp'
endif

" Do the main function
"
" Both functions simply are wrappers to run the Python commands
" Technically, the OnEnter function is unnecessary since file executes once
" But just in case, it will be assigned to the proper autocommand
"
pythonx import ultisnipsauto.tempvim as tempvim
pythonx tempvim.make_temp_root_folder()

function UltiSnips_Automate_OnEnter()
	if !isdirectory(g:ua_temp_folder)
		" Last-ditch attempt to make a temp folder
		silent !mkdir /tmp/ultisnips_automate_temp_root_dir > /dev/null 2>&1
		let g:ua_temp_folder = "/tmp/ultisnips_automate_temp_root_dir"
	endif
endfunction

function UltiSnips_AutoMate_DeleteFolder(folder)
pythonx << EOF
import shutil
# ghetto way to delete non-empty folder recursively
shutil.rmtree(vim.eval("a:folder"))
print(vim.eval("a:folder"))
EOF
endfunction

" The other pair to execute the proper clean-up code on-exit
function UltiSnips_Automate_OnLeavePre()
	if isdirectory(g:ua_temp_folder)
		call UltiSnips_AutoMate_DeleteFolder(g:ua_temp_folder)
	endif
endfunction

" Assign the functions to Vim's load/close
augroup UltiSnips_AutoMate_LoadConfig
    au!
	au VimEnter * :call UltiSnips_Automate_OnEnter()
	au VimLeavePre * :call UltiSnips_Automate_OnLeavePre()
augroup END
