#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Gets style guide information and uses it to change string objects.

description
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


filename
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
format.py

'''

# IMPORT STANDARD LIBRARIES
import inspect
from abc import ABCMeta, abstractmethod
from itertools import izip

# IMPORT THIRD-PARTY LIBRARIES
import styling


class DocstringCompositeFormatter(object):

    '''My personal preference for formatting docstrings'''

    _singleLineImpPrefix = 'single_line_imp_'

    def __init__(self, text, rule):
        super(DocstringCompositeFormatter, self).__init__()
        self.origText = text
        self.text = text
        self.rule = styling.DocstringCompositeStyle(self.text)

        if self.certify_all_rule_implementations():
            raise NotImplementedError(
                'Class {} cannot be used. It is missing '
                'the following functions, '
                '{}'.format(self.__class__.__name__,
                            self.certify_all_rule_implementations()))
    # end __init__

    @classmethod
    def get_rule_name(cls, fullRuleName):
        '''Removes the any method prefixes and returns the name of the func.

        Args:
            fullRuleName (str): The full name of the function to get its name

        '''
        if fullRuleName.startswith(cls._singleLineImpPrefix):
            return fullRuleName[len(cls._singleLineImpPrefix):]
    # end get_rule_name

    def single_line_imp_001_force_start_line(self, dictH):
        '''The start line must always contain characters.'''
        startingLineNum = self.rule.single_line_rule_001_force_start_line(
                dictH['modifiedLineText'])
        dictH['modifiedLineText'] = dictH['modifiedLineText'][startingLineNum:]
    # end single_line_imp_001_force_start_line

    def single_line_imp_001_start_line_ending(self, dictH):
        '''.'''
        commonFName = self.get_rule_name(inspect.currentframe().f_code.co_name)
        if not dictH[commonFName]['score']:
            dictH['modifiedLineText'][0] += '.'
    # end single_line_imp_001_start_line_ending

    def single_line_imp_001_end_line(self, dictH):
        '''.'''
        commonFName = self.get_rule_name(inspect.currentframe().f_code.co_name)
        if dictH[commonFName]['score'] != 0.0:
            dictH['modifiedLineText'][-1] += '\n'
        if dictH[commonFName]['score'] > 0.5:
            dictH['modifiedLineText'][-1] += '\n'
    # end single_line_imp_001_end_line

    def certify_all_rule_implementations(self):
        '''Ensures that, every rule in a style guide has an implementation.'''
        # CJK : Eventually go back and re-implement this as abstractmethods
        missingImplementations = []
        # Check if there are any rules in the rule class
        if not self.rule.singleLineRules():
            return

        # in the event that there are no implementation methods
        ruleFullNames, _ = izip(*self.rule.singleLineRules())
        if self.rule.singleLineRules() and not self.singleLineImps():
            return [self.rule.get_rule_name(x) for x in ruleFullNames]

        # Tests how many of the style guide rules were implemented
        impFullNames, _ = izip(*self.singleLineImps())
        impFullRelNames = [self.get_rule_name(x) for x in impFullNames]
        ruleFullRelNames = [self.rule.get_rule_name(x) for x in ruleFullNames]
        for ruleName in ruleFullRelNames:
            if ruleName not in impFullRelNames:
                missingImplementations.append(ruleName)
        return missingImplementations
    # end certify_all_rule_implementations

    def singleLineImps(self):
        return styling._get_methods_from_prefix(self, self._singleLineImpPrefix)
    # end singleLineImps

    def complex_format(self, dictH):
        '''Run that which takes multiple lines and/or changes the line order.'''
        # CJK : :TODO: Develop a system so that I can safely iterate over keys
        # in the dictionary and simply add/append to the final dict without
        # changing the size of the dict while iterating
        pass
    # end complex_format

    def conform_to_format(self, includeStrMarks=False):
        '''Takes the current rules and applies them to some given text.

        Returns:
            str : The final formatted string

        '''
        self.rule.serialized['modifiedLineText'] = \
                self.rule.serialized['origLineText']
        self.single_line_imp_001_force_start_line(
                self.rule.serialized)
        self.single_line_imp_001_start_line_ending(self.rule.serialized)
        self.single_line_imp_001_end_line(self.rule.serialized)
        self.complex_format(self.rule.serialized)

        output = '\n'.join(self.rule.serialized['modifiedLineText'])
        if includeStrMarks:
            try:
                output = self.rule._docstringWraps + output + \
                         self.rule._docstringWraps
            except AttributeError:
                pass

        return output
    # end conform_to_format
# end DocstringCompositeFormatter


def test_formatter():
    '''temp function.'''
    someText = '''\

something with text

another thing'''
    someText = \
'''

some docstring

anotherline'''


    fmatter = DocstringCompositeFormatter(someText, styling.DocstringCompositeStyle)
    newText = fmatter.conform_to_format()
    print newText
    # import difflib
    # diff = difflib.Differ()
    # newText = newText.split('\n')
    # newText.insert(1, '------')
    # comp = difflib.ndiff(someText.split('\n'), newText)
    # for x in comp:
    #     code, originalText = x[:2], x[2:]
    #     print code, originalText
# end test_formatter


if __name__ == '__main__':
    test_formatter()

