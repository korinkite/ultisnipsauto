#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''UltiSnips helper classes/functions to assist making snippets.

description
===============================================================================

Classes and Functions

'''

# IMPORT STANDARD LIBRARIES
import os
import sys
import re

# IMPORT THRID-PARTY LIBRARIES
try:
    from UltiSnips import UltiSnips_Manager as UltisnipsManager
except ImportError:
    from UltiSnips import SnippetManager as UltisnipsManager
from UltiSnips.snippet_manager import err_to_scratch_buffer
import px.snippets
import six
import vim


# CURRENT_SHIFT_WIDTH = vim.eval('g:snipShiftWidth')
CURRENT_SHIFT_WIDTH = 4  # CJK : :REMOVE: :LATER
ARG_SEP = ','


def add_to_nested_dict(dictH, key, item, insertAtIndex=None, excludedKeys=[]):
    '''Iterates over a dict and adds an existing key to its nested dicts.

    Args:
        key (multi): A valid dictionary key
        item (multi): A valid dictionary item(s)

    '''
    for k, itm in dictH.iteritems():
        if not isinstance(itm, dict):
            continue
        if k in excludedKeys:
            continue

        if itm.get(key) is not None and insertAtIndex is not None:
            itm[key].insert(insertAtIndex, item)
        if itm.get(key) is not None:
            itm[key] += item
        elif itm.get(key) is None:
            itm[key] = item
# end add_to_nested_dict


def parse_args(infoString, exclude=['self', 'cls'], sepWithSpaces=True):
    '''parse_args does something.

    Args:
        infoString (str): The args to parse
        exclude (list): Everything to exclude from the output
    '''
    arg_list = [x.strip() for x in infoString.split(ARG_SEP)
                if x.strip() not in exclude]
    if sepWithSpaces:
        arg_list = [' ' + x if i != 0 else x for i, x in enumerate(arg_list)]
    return ','.join(arg_list)
# end parse_args


def get_type_from_string(obj):
    '''
    Return a native python object from a string, if any

    :AUTHORNOTE: This is the ONE time I'll use eval
    '''
    try:
        objType = eval(obj)
    except:
        return None

    # ghetto strip of the object type (regex was needlessly slow)
    objType = str(type(objType))[7:-2]
    return objType
# end get_type_from_string


class Arg(object):

    '''Wrapper around a string object that represents an arg in Python.'''

    def __init__(self, argInput):
        '''Splits the string up into its various parts for better parsing.'''
        super(Arg, self).__init__()
        self._defaultvalue = None
        self._argname = None
        splitArg = [x.strip() for x in argInput.split('=')]
        self.arg = splitArg[0]

        if len(splitArg) == 2 and splitArg[1].strip() != '':
            self.defaultValue = splitArg[1].strip()
        elif len(splitArg) > 2:
            raise ValueError('Arg: {} contains more than one '
                             '"=" sign'.format(argInput))
    # end __init__

    @classmethod
    def has_default(cls, argInput):
        '''Checks if the current instance has a default argument.

        Args:
            argInput (str): The argument to check for a default value

        Returns:
            bool: Whether or not the argument has a default value

        '''
        try:
            if cls.defaultValue is None:
                return False
            return True
        except AttributeError:
            splitArg = argInput.split('=')
            if len(splitArg) < 2 \
                    or argInput.split('=')[1].strip() == '':
                return False
            elif len(splitArg) >= 2:
                raise ValueError('Arg: {} contains more than one '
                                 '"=" sign'.format(argInput))
            return True
    # end hasDefault

    @property
    def defaultValue(self):
        '''Gets the default value of the arg, if it has one.

        Returns:
            str or NoneType : self._defaultvalue or None, if no default value.

        '''
        return self._defaultvalue
    # end defaultValue.property

    @defaultValue.setter
    def defaultValue(self, value):
        '''Sets the default. Also ensures '=' in __str__ print.

        Args:
            value (multi): Sets the defaultValue of the arg

        '''
        self._defaultvalue = value
    # end defaultValue.setter

    @defaultValue.deleter
    def defaultValue(self):
        '''Deleter method.'''
        del self._defaultvalue
    # end defaultValue.deleter

    def  __str__(self):
        '''Prints the current instance.

        Returns:
            str: A humna-friendly representation of the arg class

        '''
        if self.defaultValue is not None:
            return '{}={}'.format(self.arg, self.defaultValue)
        else:
            return self.arg
    # end __str__
# end Arg


class ArgList(object):

    '''A collection of arguments, typically separated by ','s.'''

    def __init__(self, argList):
        '''Creates the arg_list that stores all the individual args.'''
        super(ArgList, self).__init__()
        self._arglist = argList
    # end __init__

    def arg_list(self, exclude=['self', 'cls']):
        return [Arg(x.strip()) for x in self._arglist.split(ARG_SEP)
                if x.strip() not in exclude]

    def num_args(self, *args, **kwargs):
        '''Simple wrapper to get the number of args.

        Note:
            Any args passed to num_args must be valid for arg_list

        '''
        return len(self.arg_list(*args, **kwargs))
    # end num_args

    def required_args(self):
        '''Gets the required arguments from the arg_list.'''
        return [x for x in self.arg_list() if x.defaultValue is None]
    # end required_args

    def __iter__(self):
        '''Iterates through the entire self.arg_list.'''
        return iter(self.arg_list)
    # end
# end ArgList


class Finder(object):

    '''Base class, meant to be subclassed for differerent filetypes.'''

    def get_last_function(self, line, match='def'):
        '''Used to search if the given line is a function definition.'''
        if match in line:
            return line
    # end get_last_function
# end Finder


class FinderPy(Finder):

    '''Functions for identifying/parsing python code for UltiSnips snippets.'''

    def __init__(self):
        self.functionHeaderMatch = '^([\t ]+)?(?P<keyType>def|class)([ ]+)?(?P<functionName>[a-zA-Z_0-9]+)([ ]+)?\('
        self.propertyMatch = '^([\t ]+)?@(?P<propertyName>.+)$'
        self.sysPropertyTypes = ['property', 'setter', 'deleter']
        self.isSysProperty = False
    # end __init__

    def get_object_name(self, line, obeyIndents=False, keyType='def'):
        functionName = re.match(self.functionHeaderMatch, line)
        if (functionName is not None and type == 'def') or \
                functionName is not None and functionName.group('keyType') == keyType:
                    return functionName.group('functionName')
    # end get_object_name

    @property
    def property(self):
        return self._property
    # end property.property

    @property.setter
    def property(self, value):
        propertyType = re.match(self.propertyMatch, value)

        if propertyType is None:
            self._property = None
            self.isSysProperty = False
            return

        propertyType = propertyType.group('propertyName')

        if propertyType is None:
            self._property = None
            self.isSysProperty = False
            return

        # search every element for property types
        propertyType = propertyType.split('.')
        propertyType = [p.strip() for p in propertyType]  # clean up names

        for p in propertyType:
            if p in self.sysPropertyTypes:
                self._property = p
                self.isSysProperty = True
                break
        else:
            self._property = value
            self.isSysProperty = False
    # end property.setter

    @property.deleter
    def property(self):
        del self._property
    # end property.deleter
    # end property
# end FinderPy


class TextFormatHelper(object):
    def __init__(self):
        super(TextFormatHelper, self).__init__()
    # end __init__

    def spacing_to_shift(numSpaces):
        try:
            int(numSpaces)
        except:
            raise
        return numSpaces / int(vim.eval('g:meshiftwidth'))
    # end spacing_to_shift
# end TextFormatHelper


class DocstringHelper(object):
    def __init__(self):
        super(DocstringHelper, self).__init__()
        self.column = int(vim.eval('col(".")'))
        self.columnSpacing = ' ' * self.column
        self.offset = int(CURRENT_SHIFT_WIDTH) + 1
        self.offsetSpacing = ' ' * self.offset
        self.ignoredArguments = ['']
    # end __init__

    def format_items(self, tNum, sep=','):
        arguments = t[tNum].split(sep)
        arguments = [argument.strip() for argument in arguments if argument]

        offsetFull = self.columnSpacing + self.offsetSpacing

        if self.hasItems:
            # filter out items that should be ignored, like 'self'
            # split args up and add any formatting info needed.
            items = [x for x in arguments if x not in self.ignoredArguments]
            items = [self.item_line.format(x) for x in items]
            if len(items) == 0:
                return None

            return items
        elif not self.hasItems and arguments is not None:
            # outputString = self.header + str('\n' + offsetFull) + arguments[0]
            # return outputString
            return ':AUTHORNOTE: Implement'
        else:
            return None
    # end format_items

    @property
    def item_line(self):
        '''
        Getter method
        '''
        return self._itemLine
    # end item_line.property

    @item_line.setter
    def item_line(self, value):
        '''
        Setter method
        '''
        self._itemLine = value
    # end item_line.setter
    # end item_line
# end DocstringHelper


class DocstringHelperArgs(DocstringHelper):
    def __init__(self):
        super(DocstringHelperArgs, self).__init__()
        self.header = 'Args:'
        self.hasItems = True
        self.item_line = '{} ({}): '
        self.defaultDocstringType = ''
        # self.defaultArgRegex = '^(?P<argName>[a-zA-Z0-9]+)  (  [ ]+?     =     [ ]+?      (?P<argDefaultValue>.+)?)?$'
        self.defaultArgRegex = '''
                               ^(?P<argName>[a-zA-Z0-9]+)
                               ([\ ]+)?  # optional space
                               (  # optional default arg group start
                                  =  # required = sign
                                  (?P<argDefaultValue>
                                    (
                                      ([\ ]+)?  # optional space
                                    )
                                  .+)?  # allow any characters for default arg
                               )?$  # optional default arg group end
                               '''

    # end __init__

    def get_type(self, item, regex=None):
        '''Expects a variable name = something. Extracts the something and gets
        its type.

        If something doesn't exist or its type cannot be determined,
        returns None

        '''
        if regex is None:
            reCompile = re.compile(self.defaultArgRegex, re.VERBOSE)
        else:
            reCompile = regex

        reMatch = re.match(reCompile, item)
        if reMatch is None:
            return None

        argName = reMatch.group('argName')
        argType = reMatch.group('argDefaultValue')

        if argType is not None:
            argType = get_type_from_string(argType)
        if argType is None:
              argType = ''
        else:
            argType = ''

        return (argName, argType)
    # end get_type

    def format_items(self, tNum, sep=',', arguments=[]):
        '''
        '''
        arguments = [argument.strip() for argument in arguments if argument]

        offsetFull = self.columnSpacing + self.offsetSpacing

        if self.hasItems:
            # filter out items that should be ignored, like 'self'
            # split args up and add any formatting info needed.
            #
            items = [x for x in arguments if x not in self.ignoredArguments]

            if len(items) == 0:
                return None
            # if any of the items have a default value, get its type
            # if they don't have default values, pass '' and continue anyway
            #
            itemType = []
            reCompile = re.compile(self.defaultArgRegex, re.VERBOSE)
            for index, item in enumerate(items):
                itemInfo = self.get_type(item, reCompile)
                items[index] = itemInfo

            items = [self.item_line.format(x[0], x[1]) for x in items]

            return items
        elif not self.hasItems and arguments is not None:
            # outputString = self.header + str('\n' + offsetFull) + arguments[0]
            # return outputString
            return ':AUTHORNOTE: Implement'
        else:
            return None
# end DocstringHelperArgs


class DocstringHelperReturn(DocstringHelper):
    def __init__(self):
        super(DocstringHelperReturn, self).__init__()
        self.header = 'Returns:'
        self.hasItems = False
        self.item_line = '{}: {}'

        returnRegex = '^([\t\r\n ]+)?return (?P<returnObj>.+)?$'
        self.returnCompile = re.compile(returnRegex)
    # end __init__

    def format_items(self, tNum, lines):
        for line in lines:
            reMatch = re.match(self.returnCompile, line)
            if reMatch is not None:
                break
        else:
            return None

        returnName = reMatch.group('returnObj')
        returnType = get_type_from_string(returnName)

        if returnType is None:
            returnType = ''

        outputString = self.item_line.format(returnType, returnName)
        return outputString
    # end format_items
# end DocstingHelperReturn


"""
class UltiSnipsManagerExtended(UltisnipsManager):

    '''Adds extra functions without modifying the existing UltiSnips API.'''

    @err_to_scratch_buffer
    def list_snippets_objects(self):
        '''Same as the list_snippets function but returns snippet objects.
k
        list_snippets originally only returned a bool and couldn't be used
        to search within the snippets for specific snippets.

        '''
        before = _vim.buf.line_till_cursor
        snippets = self._snips(before, True)
        return snippets
    # end list_snippets_objects
# end UltiSnipsManagerExtended
"""


def expand_snippet(snip, jump_pos=None):
    '''Expands snippet at position, 'jump_pos'. Expands all if set to None.'''
    if snip.tabstop != jump_pos and jump_pos is not None:
        return

    vim.eval('feedkeys("\<C-R>=UltiSnips#ExpandSnippet()\<CR>")')
# end expand_snippet


def complete(t, opts):
    '''
    t - the text of the tabstop to complete
    opts - the options to complete t with
    '''
    if t:
        # filter out options that DON'T start with what has been typed
        opts = [m[len(t):] for m in opts if m.startswith(t)]
    if len(opts) == 1:
        return opts[0]
    return '({})'.format('|'.join(opts))
# end complete


def underscore_to_camelcase(path):
    func = lambda s: s[:1].lower() + s[1:] if s else ''

    lastPart = path.split('/')[-1]
    subbedString = re.sub(r'[_\-]', '', lastPart.title())
    subbedString = str(subbedString)
    if len(subbedString) >= 1:
        subbedString = func(subbedString)
        return subbedString
# end underscore_to_camelcase


def find_snippet_by_name(snippets, name):
    '''Given a list of snippets, find the one that matches by name.

    .. note::

        Requires a list of SnippetDefinitions. Ideally, you'd get it from
        UltiSnipsManagerExtended.list_snippets_objects()

    '''
    snippet = filter(lambda x: str(x.trigger) == name, snippets)
    if snippet is not None and snippet != []:
        return snippet[0]
    else:
        return None
# end find_snippet_by_name


def get_docstring_bounds(vimLines, rowIndex=None, offsetBounds=0,
                         splitBy=os.linesep):
    '''Given the current line, search for and return the wrapped docstring.

    Args:
        rowIndex (int): The current line

    '''
    startOfDocstring = 0
    endOfDocstring = -1

    if isinstance(vimLines, six.string_types):
        vimLines = vimLines.split(os.linesep)

    if rowIndex is None:
        # CJK : Get the current line, somehow
        (rowIndex, _) = vim.current.window.cursor
        rowIndex -= 1

    for i, line in enumerate(vimLines[rowIndex:]):
        if "'''" in line or '"""' in line:
            endOfDocstring = i + rowIndex + 1

    for i, line in enumerate(reversed(vimLines[:rowIndex])):
        if "'''" in line or '"""' in line:
            startOfDocstring = rowIndex - i

    if startOfDocstring - offsetBounds >= 0:
        startOfDocstring -= offsetBounds

    if endOfDocstring + offsetBounds <= len(vimLines):
        endOfDocstring += offsetBounds

    return (startOfDocstring - offsetBounds, endOfDocstring)
# end get_docstring_bounds


if __name__ == '__main__':
    print(__doc__)
