#!/usr/bin/env python
# -*- coding: utf-8 -*-


'''Contains style-guide information (like PEP-257, NumPy, Sphinx, etc)

description
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
This module contains 'model' information about different style-guide options.
It does NOT contain methods for manipulating this data. See format.py for that.

The classes are organized as follows
General classes
- Docstring classes (uses the general classes)
 - Each docstring class implementation contains rules from their style-guides
- General Style Guides
 - Contains info about the docstring and other style-guide rules
 - In general, these classes have features added as-needed and are not 100%

Note
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Some style guides can be combined and resolved reasonably. For example,
PEP-257 and GStyle, while not mutually exclusive, do not have conflicting
instructions.

filename
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
styling.py

'''

# IMPORT STANDARD LIBRARIES
import re
import os
import inspect
import textwrap
from collections import defaultdict, MutableMapping

# IMPORT LOCAL LIBRARIES
import docstring


BASE_DICT = {'overallScore': 0,
             'highestScore': 0,
             'origText': '',
             'origLineText': [],
             'fixLines': defaultdict(list),
             'version': (1, 0)}


class TransformedDict(MutableMapping):

    '''Shuffles dict keys when adding new keys or deleting older ones.'''

    def __init__(self, *args, **kwargs):
        self.store = dict()
        self.update(dict(*args, **kwargs))  # use the free update to set keys
    # end __init__

    def __getitem__(self, key):
        return self.store[key]
    # end __getitem__

    def __setitem__(self, key, value):
        self.store[key] = value
    # end __setitem__

    def __delitem__(self, key):
        del self.store[key]

        self.store = {(k if k < key else k-1): itm
                      for k, itm in self.dictH.iteritems()}
    # end __delitem__

    def __iter__(self):
        return iter(self.store)
    # end __iter__

    def __len__(self):
        return len(self.store)
    # end __len__
# end TransformedDict


def _get_methods_from_prefix(classObj, prefix):
    '''Gets all methods from a class if their names start with prefix.'''
    return [x for x
            in inspect.getmembers(classObj, predicate=inspect.ismethod)
            if x[0].startswith(prefix)]
# end _get_methods_from_prefix


class DocstringStyle(object):

    '''The most generic docstring type.'''

    _singleLineRulePrefix = 'single_line_rule_'
    _firstLineEndsWith = '.'
    minimumPercent = 0.8

    def __init__(self, text):
        super(DocstringStyle, self).__init__()
        self.origText = text
        self.text = text
        self.origLines = self.origText.split('\n')
        self.lineWidth = 79
    # end __init__

    @classmethod
    def get_rule_name(cls, fullRuleName):
        '''Removes the any method prefixes and returns the name of the func.

        Args:
            fullRuleName (str): The full name of the function to get its name

        '''
        if fullRuleName.startswith(cls._singleLineRulePrefix):
            return fullRuleName[len(cls._singleLineRulePrefix):]
    # end get_rule_name
# end DocstringStyle


class DocstringPep257(DocstringStyle):

    '''Contains information about PEP-257 docstrings.'''

    serialized = BASE_DICT.copy()

    def __init__(self, text, info=[]):
        '''Gives the ability to add information about the docstring.

        One bit of info that is useful to know is if the docstring is a class.
        PEP257 requires that classes have a one-line upper and lower margin.

        Args:
            text ()
            info (list)

        '''
        super(DocstringPep257, self) .__init__(text)
        self.serialized.update({self.get_rule_name(x[0]): {} for x
            in inspect.getmembers(self, predicate=inspect.ismethod)})
        self.info = info
        self.serialized['info'] = self.info
        self.serialized['origText'] = text
        self.serialized['origLineText'] = text.split('\n')
        self.serialized['fixLines'] = defaultdict(list)
        docStart, docEnd = \
                docstring.crop_to_docstring(self.serialized['origText'])
        self.serialized['bounds'] = (0, -1)

        if not self.is_style():
            raise ValueError('text: {!r} is not a valid docstring type: '
                             '{}'.format(text, self.__class__.__name__))
    # end __init__

    def is_style(self):
        '''If the docstring meets the min-required style guide requirements.'''
        return self.get_score_text(self.text) > self.minimumPercent
    # end is_style

    def single_line_rule_001_force_start_line(self, lines):
        '''Checks if the start line contains any characters.'''
        commonFName = self.get_rule_name(inspect.currentframe().f_code.co_name)
        self.serialized[commonFName]['singleHighestScore'] = 1
        self.serialized['highestScore'] += \
                self.serialized[commonFName]['singleHighestScore']

        startingNum, startingLine = 0, ''
        for index, line in enumerate(lines):
            if line.strip():
                startingNum, startingLine = index, line
                break
        self.serialized[commonFName]['lineText'] = startingLine
        self.serialized[commonFName]['score'] = \
            float(len(lines) - startingNum) / len(lines)
        self.serialized['overallScore'] += self.serialized[commonFName]['score']

        if startingNum:
            self.serialized[commonFName]['fix'] = startingNum
            self.serialized['fixLines'][startingNum].append(commonFName)

        return startingNum
    # end single_line_rule_001_start_line_ending

    def single_line_rule_001_start_line_ending(self, line):
        '''The first line of a docstring must end with a "." character.'''
        commonFName = self.get_rule_name(inspect.currentframe().f_code.co_name)
        self.serialized[commonFName]['lineText'] = line
        self.serialized[commonFName]['singleHighestScore'] = 1
        self.serialized['highestScore'] += \
                self.serialized[commonFName]['singleHighestScore']

        if line.endswith(self._firstLineEndsWith):
            hasSuffix = 1
        else:
            hasSuffix = 0
        self.serialized[commonFName]['score'] = hasSuffix
        self.serialized['overallScore'] += \
                self.serialized[commonFName]['score']

        if not hasSuffix:
            self.serialized[commonFName]['fix'] = True
            self.serialized['fixLines'][0].append(commonFName)

        return int(hasSuffix)
    # end single_line_rule_002_start_line_ending

    def single_line_rule_001_end_line(self, lines):
        '''Adds a blank line to text if the number of lines > 1.'''
        commonFName = self.get_rule_name(inspect.currentframe().f_code.co_name)
        self.serialized[commonFName]['lineText'] = lines
        self.serialized[commonFName]['singleHighestScore'] = 1
        self.serialized['highestScore'] += \
                self.serialized[commonFName]['singleHighestScore']

        score = 0
        lastIndexWithText = next(i for i, _ in enumerate(reversed(lines)))
        if len(lines) - lastIndexWithText > 1 and \
                not lines[lastIndexWithText + 1].strip():
            score += 0.5
        if len(lines) - lastIndexWithText > 2 and \
                not lines[lastIndexWithText + 1].strip():
            score += 0.5

        self.serialized[commonFName]['score'] = score
        self.serialized['overallScore'] += \
                self.serialized[commonFName]['score']
    # end single_line_rule_001_end_line

    def singleLineRules(self):
        '''Gets all of the lines on the class that evaluate a single line.'''
        return _get_methods_from_prefix(self, self._singleLineRulePrefix)
    # end singleLineRules

    def get_score_text(self, text):
        '''Determines how closely the provided text fits the style guide.'''
        # CJK : replace with six.string_types later
        if isinstance(text, basestring):
            text = text.split('\n')
        self.single_line_rule_001_force_start_line(text)
        self.single_line_rule_001_start_line_ending(text[0])

        self.single_line_rule_001_end_line(text)
        return float(self.serialized['overallScore']) / \
                self.serialized['highestScore']
    # end get_score_text
# end DocstringPep257


ARG_LINE_GOOGLE_STYLE_RE = \
r'''^[\ t]+(?P<argName>\S+)       # the argument name
(?:\ \((?P<argType>\S+)\))?:      # the (optional) argument type
[\ ]+(?P<argDesc>[^:]+)$'''       # The argument description (separated by ':'s)
ARG_LINE_GOOGLE_STYLE_RE_COMPILE = re.compile(ARG_LINE_GOOGLE_STYLE_RE,
                                              re.VERBOSE | re.MULTILINE)


class DocstringArgGStyle(object):

    '''.'''

    def __init__(self, *args, **kwargs):
        '''.'''
        super(DocstringArgGStyle, self).__init__()
        if kwargs.get('match'):
            match = kwargs.get('match')
            if not isinstance(match, kwargs.get('matchType')):
                raise RuntimeError('Wrong type passed to class')
            self.reMatch = match

        self.type = self.reMatch.group('argType')
        self.description = self.reMatch.group('argDesc')
        self.name = self.reMatch.group('argName')
    # end __init__
# end DocstringArgGStyle


class DocstringArgListGStyle(object):

    '''.'''

    _argumentHeaderLine = ['Args:']

    def __init__(self, text):
        ''''''
        super(DocstringArgListGStyle, self).__init__()
        self.origText = text
        if not self.is_arg_list(self.origText):
            raise ValueError('Not a valid arg list')
        self.lines = text.split('\n')
    # end __init__

    @classmethod
    def is_arg_list(cls, argList):
        '''Checks if the passed information is a valid argument list.

        Args:
            argList (str): The string to check

        '''
        return argList.startswith(cls._argumentHeaderLine[0])
    # end is_arg_list

    @property
    def args(self):
        '''.'''
        matches = list(re.finditer(ARG_LINE_GOOGLE_STYLE_RE_COMPILE,
                                   '\n'.join(self.lines[1:])))
        return \
            tuple(DocstringArgGStyle(matchType=type(matches[0]), match=x)
                  for x in matches)
    # end args
# end DocstringArgListGStyle


class DocstringGStyle(DocstringStyle):

    '''Information about Google-Style docstring syntax, AKA Napolean-sphinx.'''

    _argRegex = NotImplementedError()

    def __init__(self, text):
        super(DocstringGStyle, self).__init__(text)
    # end __init__

    def format_argument_header(self, lines, index):
        '''Describes what an argument line would look like in a docstring.

        Note - regex is not used here or in many other places to ensure speed.

        '''
        argHeaderStart = self._argumentHeaderLine[0]
        if not lines[index].startswith(argHeaderStart):
            return False

        initialIndent = len(lines[index]) - len(lines[index].lstrip())

        # Args can sometimes span multiple lines if their descriptions are long
        # plan for this by collecting args if they match the correct indent
        #
        _tempArgLineCollect = []
        for textAfter in lines[index:]:
            # CJK : :TODO: Test if textwrap.dedent is safe for tabs and spaces
            argIndent = textwrap.dedent(textAfter)
            if argIndent != initialIndent:
                _tempArgLineCollect.append([textAfter])
            elif argIndent == initialIndent and _tempArgLineCollect:
                argList.append(_tempArgLineCollect + [textAfter])
            else:
                argList.append([textAfter])

            if textAfter.strip():
                break  # encountered a blank line, arg list is over

        if not argList:
            return False  # something went wrong. Args: has no args!

        for arg in argList:
            argName, argType, argDescription = self.format_argument_line(arg)

    # end format_argument_header

    def format_001_external_arg_wrapper(self):
        '''If an object is from an external module, wraps text.

        This is typically used for parsing lists of kwargs and trying to find
        their built-in types automatically.

        '''
        return ('<', '>')
    # end format_001_external_arg_wrapper

    def format_001_header_sep(self, text, initialIndent=0):
        '''Allowed header separators.

        The header is as long as it can be without going past the max width.
        Only certain header symbols are valid. The main ones are listed under
        self._headers.

        Args:
            text ():
            initialIndent (int): The number of white-space characters before
                                 the text.

        '''
        pass
    # end format_001_header_sep
# end DocstringGStyle


class DocstringSphinx(DocstringStyle):

    '''.'''

    _headers = ('=', '-', '+', '*', '#')

    def __init__(self, text):
        super(DocstringSphinx, self).__init__(text)
    # end __init__
# end DocstringSphinx


IS_CLASS_DOCSTRING = None
IS_DEF_DOCSTRING = None


"""
def single_line_rule_wrapper(cls, function):
    '''.'''
    cls.serialized[cls.get_rule_name(function.__name__)]\
            ['functionName'] = function.__name__
    return function
# end single_line_rule_wrapper


def common_serial_wrapper(cls):
    '''Add dict info to functions that tend to serialize with the same items.'''
    for (name, function) in vars(cls).items():
        if name.startswith(cls._singleLineRulePrefix):
            setattr(cls, name, single_line_rule_wrapper(cls, function))
    return cls
# end common_serial_wrapper


@common_serial_wrapper
"""
class DocstringCompositeStyle(DocstringPep257, DocstringGStyle,
                              DocstringSphinx):

    '''My personal perference when writing/formatting docstrings.'''

    _docstringWraps = "'''"

    def __init__(self, text, info=[]):
        super(DocstringCompositeStyle, self).__init__(text, info)
    # end __init__

    def single_line_rule_001_docstring_wraps(self, lines):
        '''Requires that a docstring begin and end with the specified wrap.'''
        score = 0
        if not lines[0].endswith(self._docstringWraps):
            score += 0.5
        if not lines[-1].endswith(self._docstringWraps):
            score += 0.5
    # end single_line_rule_001_docstring_wraps
# end DocstringCompositeStyle


class GStyle(object):

    '''Contains all information about Google-Style syntax.'''

    def __init__(self, text):
        '''TODO: Make a docstring'''
        super(GStyle, self).__init__(text)
        self.docstringStyle = DocstringGStyle()
    # end __init__
# end GStyle


def main():
    '''Test if inspect can be used to get and use modules.'''
    someText = '''\




something with text

another thing

'''
    docstring = DocstringCompositeStyle(someText)
# end main


def get_docstring_type(docstring):
    '''Gets the docstring's style guide, if one exists.

    The order of checking is first the composite type, then google-style,
    sphinx, then PEP257.

    Args:
        docstring (str)

    '''
    rules = (DocstringCompositeStyle, DocstringGStyle, DocstringSphinx,
             DocstringPep257)
    for rule in rules:
        docStyle = rule(docstring)
        if docStyle.get_score_text > docStyle.minimumPercent:
            return rule
# end get_docstring_type


def test_arg_parse():
    '''.'''
    someText = \
'''
A more practical example
Args:
    something (int): A description
    some1asdf (str): Some description and it spans multiple lines because the
                     description is so long
incorrectly formatted (there should have been a blank line below this)

'''
    someText = \
'''

Args:
    something (int): A description
    some1asdf (str): Some description and it spans multiple lines because the
                     description is so long
    argWithNoType: something to say about this arg
'''
    docString = DocstringArgListGStyle(someText)
    for arg in docString.args:
        print arg.name
        pass
# end test_arg_parse


def test_get_docstring_type():
    '''.'''
    exampleDocstring = \
'''something with a docstring.

another line or something

Args:
    someArg (int): Some description'''

    print(get_docstring_type(exampleDocstring))
# end test_get_docstring_type

if __name__ == '__main__':
    test_arg_parse()

