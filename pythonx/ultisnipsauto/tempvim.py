#!/usr/bin/env python
# -*- coding: utf-8 -*-

# IMPORT STANDARD LIBRARIES
import os
import tempfile

# IMPORT LOCAL LIBRARIES
import vim


def make_temp_root_folder():
    """Creates a directory - uses "g:ua_tempfolder" to save the full path."""
    tempDir = tempfile.mkdtemp()
    vim.command("let g:ua_temp_folder = '{}'".format(os.path.abspath(tempDir)))
# end make_temp_root_folder
