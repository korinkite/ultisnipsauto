'''Defines functions to create UltiSnips functions

description

'''

# IMPORT STANDARD LIBRARIES
import copy
from collections import OrderedDict

# IMPORT LOCAL LIBRARIES
# from helper import add_to_nested_dict

# CJK : :REMOVE: :LATER: :START:
def add_to_nested_dict(dictH, key, item, insertAtIndex=None, excludedKeys=[]):
    '''Iterates over a dict and adds an existing key to its nested dicts.

    Args:
        key (multi): A valid dictionary key
        item (multi): A valid dictionary item(s)

    '''
    for k, itm in dictH.iteritems():
        if not isinstance(itm, dict):
            continue
        if k in excludedKeys:
            continue

        if itm.get(key) is not None and insertAtIndex is not None:
            itm[key].insert(insertAtIndex, item)
        if itm.get(key) is not None:
            itm[key] += item
        elif itm.get(key) is None:
            itm[key] = item
# end add_to_nested_dict
# CJK : :REMOVE: :LATER: :END:

# SETUP THE ARITHMETIC TYPES
ARITHMETIC_TYPES = \
{
    # Arithmetic Operators
    'add':
    {
        'symbol': '+',
    },
     'and':
    {
        'symbol': '&',
    },
    'div':
    {
        'symbol': '/',
    },
    'divmod':
    {
        'symbol': 'divmod()',
    },
    'floordiv':
    {
        'symbol': '//',
    },
    'lshift':
    {
        'symbol': '<<',
    },
    'mul':
    {
        'symbol': '*',
    },
    'mod':
    {
        'symbol': '%',
    },
    'or':
    {
        'symbol': '|',
    },
    'pow':
    {
        'symbol': '**',
    },
    'rshift':
    {
        'symbol': '>>',
    },
    'sub':
    {
        'symbol': '-',
    },
    'truediv':
    {
        'symbol': '/',
        'requires': 'from __future__ import division  # py 2.2+'
    },
    'xor':
    {
        'symbol': '^',
    },
}
add_to_nested_dict(ARITHMETIC_TYPES, 'args',
                          OrderedDict([('self', {}), ('other', {})]),
                          insertAtIndex=0, excludedKeys=['pow'])

REFLECTIVE_ARITMETIC_TYPES = copy.deepcopy(ARITHMETIC_TYPES)
REFLECTIVE_ARITMETIC_TYPES = \
{'r' + k: v for k, v in REFLECTIVE_ARITMETIC_TYPES.iteritems()}
add_to_nested_dict(REFLECTIVE_ARITMETIC_TYPES, 'type',
                          'reflected arithmetic')

AUGMENTED_ASSIGNMENT_TYPES = copy.deepcopy(ARITHMETIC_TYPES)
AUGMENTED_ASSIGNMENT_TYPES = \
{'i' + k: v for k, v in AUGMENTED_ASSIGNMENT_TYPES.iteritems()}
for key, item \
        in AUGMENTED_ASSIGNMENT_TYPES.iteritems():
    item['type'] = 'augmented assignment'
    # Add a '=' to the end of every symbol
    item['symbol'] += '='

add_to_nested_dict(ARITHMETIC_TYPES, 'type', 'arithmetic')

COMPARISON_TYPES = \
{
    # Comparison Operators
    'cmp':
    {
        'symbol': 'multiple',
        'example': 'self == other, self > other, etc.'
    },
    'eq':
    {
        'symbol': '==',
    },
    'ge':
    {
        'symbol': '>=',
    },
    'gt':
    {
        'symbol': '>',
    },
    'le':
    {
        'symbol': '<=',
    },
    'lt':
    {
        'symbol': '<',
    },
    'ne':
    {
        'symbol': '!=',
    },
}
add_to_nested_dict(COMPARISON_TYPES, 'type', 'comparison')
add_to_nested_dict(COMPARISON_TYPES, 'args',
                          OrderedDict([('self', {}), ('other', {})]),
                          insertAtIndex=0)

UNARY_TYPES = \
{
    # Unary Operators
    'abs':
    {
        'symbol': 'abs()',
    },
    'ceil':
    {
        'symbol': 'math.ceil()',
    },
    'floor':
    {
        'symbol': 'math.floor()',
    },
    'invert':
    {
        'symbol': '~',
        'example': '~self'
    },
    'pos':
    {
        'symbol': '+',
        'example': '+self'
    },
    'neg':
    {
        'symbol': '-',
        'example': '-self'
    },
    'round':
    {
        'args': ['n'],
        'symbol': 'round()',
    },
    'trunc':
    {
        'symbol': 'math.trunc()',
        'used by': ['unary', 'conversion']
    },
}
add_to_nested_dict(UNARY_TYPES, 'type', 'unary')
add_to_nested_dict(UNARY_TYPES, 'args',
                          OrderedDict([('self', {}), ('other', {})]),
                          insertAtIndex=0)

CONVERSION_TYPES = \
{
    # Conversion Operators
    'coerce': {},
    'complex': {},
    'float': {},
    'hex': {},
    'index':
    {
        'example': 'x[self]'
    },
    'int': {},
    'long': {},
    'trunc': {},
    'oct': {},
}
add_to_nested_dict(CONVERSION_TYPES, 'type', 'conversion')
add_to_nested_dict(CONVERSION_TYPES, 'args', OrderedDict([('self', {})]),
                          insertAtIndex=0)

REPRESENTATION_TYPES = \
{
    # Representation Operators
    'dir': {},
    'format':
    {
        'args': ['formatstr'],
    },
    'hash': {},
    'nonzero':
    {
        'example': 'bool(self)'
    },
    'bool':
    {
        'example': 'bool(self)'
    },
    'repr': {},
    'sizeof': {},
    'str': {},
    'unicode': {},
    'bytes':
    {
        'requires': 'py 3.X+'
    }
}
add_to_nested_dict(REPRESENTATION_TYPES, 'type', 'representation')
add_to_nested_dict(REPRESENTATION_TYPES, 'args', OrderedDict([('self', {})]),
                          insertAtIndex=0)

ACCESS_TYPES = \
{
    # Attribute Access Operators
    'delattr':
    {
        'example': 'del self.name'
    },
    'getattr':
    {
        'example': "self.name # name does'nt exist"
    },
    'getattribute':
    {
        'example': 'self.name'
    },
    'setattr':
    {
        'args': ['value'],
        'example': 'self.name = val'
    },
}
add_to_nested_dict(ACCESS_TYPES, 'type', 'access')
add_to_nested_dict(ACCESS_TYPES, 'args', OrderedDict([('self', {}), ('name', {})]),
                          insertAtIndex=0)

CONTAINER_TYPES = \
{
    # Container Operators
    'contains':
    {
        'args': ['item'],
        'example': 'value in self, value not in self'
    },
    'delitem':
    {
        'args': ['key'],
        'example': 'del self[key]'
    },
    'getitem':
    {
        'args': ['key'],
        'example': 'self[key]'
    },
    'iter':
    {
        'example': 'for x in self'
    },
    'len': {},
    'missing':
    {
        'args': ['key'],
    },
    'reversed': {},
    'setitem':
    {
        'args': ['key', 'value'],
        'example': 'self[key] = val'
    }
}
add_to_nested_dict(CONTAINER_TYPES, 'type', 'container')
add_to_nested_dict(CONTAINER_TYPES, 'args', OrderedDict([('self', {})]),
                          insertAtIndex=0)

DESCRIPTOR_TYPES = \
{
    'delete': {},
    'get':
    {
        'args': ['owner'],
    },
    'set':
    {
        'args': ['value']
    }
}
add_to_nested_dict(DESCRIPTOR_TYPES, 'args',
                          OrderedDict([('self', {}), ('instance', {})]),
                          insertAtIndex=0)

add_to_nested_dict(DESCRIPTOR_TYPES, 'type', 'descriptor')

COPYING_TYPES = \
{
    'copy': {},
    'deepcopy':
    {
        'args': OrderedDict([('memodict', {'defaultValue': {}})])
    }
}
add_to_nested_dict(COPYING_TYPES, 'type', 'copying')

PICKLING_TYPES = \
{
    'getinitargs': {},
    'getnewargs': {},
    'getstate':
    {
        'example': 'pickle.dump(pkl_file, self)'
    },
    'reduce': {},
    'reduce_ex': {},
    'setstate':
    {
        'args': ['state'],
        'example': 'data = pickle.load(pkl_file)'
    }
}
add_to_nested_dict(PICKLING_TYPES, 'type', 'pickling')
add_to_nested_dict(PICKLING_TYPES, 'args', OrderedDict([('self', {})]),
                          insertAtIndex=0)

ALL_TYPES = \
{
    'del':
    {
        'symbol': 'del',
        'type': 'delete',
    },
    'instancecheck':
    {
        'args': ['instance'],
        'type': 'reflection instance'
    },
    'subclasshook':
    {
        'args': ['subclass'],
        'type': 'reflection subclass'
    },
    'call':
    {
        'args': ['*args'],
        'example': 'self(args)',
        'type': 'callable objects'
    },
    'enter':
    {
        'example': 'with self as x:',
        'type': 'context'
    },
    'exit':
    {
        'args': ['exception_type', 'exception_value', 'traceback'],
        'example': 'with self as x:',
        'type': 'context'
    }
}

add_to_nested_dict(ALL_TYPES, 'args', OrderedDict([('self', {})]),
                          insertAtIndex=0)

# Collect all the various types into on large descriptor dictionary
ALL_TYPES.update(ARITHMETIC_TYPES)
ALL_TYPES.update(REFLECTIVE_ARITMETIC_TYPES)
ALL_TYPES.update(AUGMENTED_ASSIGNMENT_TYPES)
ALL_TYPES.update(COMPARISON_TYPES)
ALL_TYPES.update(UNARY_TYPES)
ALL_TYPES.update(CONVERSION_TYPES)
ALL_TYPES.update(REPRESENTATION_TYPES)
ALL_TYPES.update(ACCESS_TYPES)
ALL_TYPES.update(CONTAINER_TYPES)
ALL_TYPES.update(PICKLING_TYPES)


class DefinitionPy(object):

    '''A class describing the top line of a Python method.'''

    def __init__(self, name, args, argSep=', '):
        super(DefinitionPy, self).__init__()
        self.name = name
        self.args = args
        self.argSep = argSep
    # end __init__

    def asSnippet(self):
        '''Returns the instance as it would appear as a snippet.'''
        pass
    # end asSnippet

    def __str__(self):
        '''Creates a printable representation of the current instance.'''
        return 'def {}({}):'.format(self.name, ', '.join(self.args))
    # end __str__
# end DefinitionPy


class DescriptorDefinitionPy(DefinitionPy):

    '''Definition of the top line of a descriptor in Python.'''

    def __init__(self, name, args):
        super(DescriptorDefinitionPy, self).__init__(name, args)
        if not self.name.startswith('__') and not self.name.endswith('__'):
            self.name = '__{}__'.format(self.name)  # wrap in __s
    # end __init__
# end DescriptorDefinitionPy


class SnippetDefinition(object):

    '''General information for an UltiSnips snippet.'''

    def __init__(self):
        super(SnippetDefinition, self).__init__()
        self._sections = []
    # end __init__

    def add_section(self, section, separator=None):
        '''Add blocks of text to a final snippet definition.

        Args:
            section (function): The function to add

        Returns:
            bool or NoneType: The success or failure of the add

        '''
        self._sections.append(section)
        return True
    # end add_section

    def add_sections(self, sections, separator=None):
        '''Iterator convenience method for self.add_section

        Args:
            separator (NoneType)

        Returns:
            : self.add'''
        for section in sections:
            self.add_section(section, separator=separator)
    # end add_sections
# end SnippetDefinition


class DescriptorSnippet(SnippetDefinition):

    '''Snippet methods for defining a Python descriptor.'''

    def __init__(self, name, descriptor):
        '''Gets and parses information about a dsecriptor.'''
        super(DescriptorSnippet, self).__init__()
        self._rawData = descriptor
        self.name = name
        if not self.name.startswith('__') and not self.name.endswith('__'):
            self.name = '__{}__'.format(self.name)  # wrap in __s

        self.args = descriptor.get('args')
        self.example = descriptor.get('example')
        self.type = descriptor['type']

        # Construct the descriptor snippet
        self.add_sections([self.definition_header, self.function_body,
                           self.definition_footer])
    # end __init__

    def definition_header(self):
        '''The top-most part of the snippet.

        Returns:
            str : The top of the snippet

        '''
        return DefinitionPy(self.name, self.args.keys())
    # end definition_line

    def definition_footer(self):
        '''The bottom of the function - typically is considered optional.'''
        return '# end {}'.format(self.name)
    # end definition_footer

    def function_body(self):
        '''The actual body of the function.

        Returns:
            str: The middle of the function

        '''
        return 'something'
    # end function_body

    def build_snippet(self):
        '''Gets printable string, given the instance's current information.

        Returns:
            str : The output string of the current snippet

        '''
        return self._sections
    # end build_snippet

    def __str__(self):
        '''Prints the current snippet.

        Returns:
            str : The output string of the current snippet

        '''
        return self.build_snippet()
    # end __str__
# end DescriptorSnippet


def build_descriptor_snippets():
    '''Makes all of the Python descriptor snippets.'''
    snippet = 'def __{}__({}):\n\tself.$1 += other.$1\n# end __{}__'\
              ''.format('iadd', ', '.join(ALL_TYPES['iadd']['args'].keys()),
                        'iadd')

    descriptorSnippet = DescriptorSnippet('iadd', ALL_TYPES['iadd'])
    print descriptorSnippet
# end build_descriptor_snippets

build_descriptor_snippets()

"""
if __name__ == '__main__':
    print(__doc__)
"""






