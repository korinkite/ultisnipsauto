#!/usr/bin/env python
# -*- coding: utf-8 -*-

# IMPORT STANDARD LIBRARIES
import os
import pickle
import shutil

# IMPORT THIRD-PARTY LIBRARIES
import vim


def find_snippet_by_name(snippets, name):
    """Given a list of snippets, find the one that matches by name"""
    snippet = filter(lambda x: str(x.trigger) == name, snippets)
    if snippet is not None and snippet != []:
        return snippet[0]
    else:
        return
# end find_snippet_by_name


class Document(object):

    """All file-level operations are collected/executed in this class.

    .. important::

        Requires a Vim variable, g:ua_temp_folder, to return
        an absolute path to store the cached files.

        Must also have an environment variable, _VIMHOME, defined.
        _VIMHOME/UltiSnips must contain all your snippet definitions.

    """

    _snippet_dir = os.path.join(os.environ["VIM_HOME"], "UltiSnips")
    _cached_root_dir = vim.eval("g:ua_temp_folder")
    _cached_snippet_file = os.path.join(_cached_root_dir, "current.snippets")
    _pickled_snippet_defs = os.path.join(_cached_root_dir, "snippets.pickle")

    def __init__(self):
        """Creates caches for all the snippet files."""
        [self.copy_snippet_file(os.path.join(self._snippet_dir, x))
         for x in os.listdir(self._snippet_dir)]
    # end __init__

    def is_updated_cache_file(self, fileH):
        """Checks if the cached/pickle file is as current as the refered file.

        The directory of the cached file is hard-coded into the class and so it
        should not be referenced directly.

        Args:
            fileH (str): The full path to the current file (NOT the cache) to check

        """

        fileName = os.path.basename(fileH)
        cachedFileH = os.path.join(self._cached_root_dir, fileName)

        if not os.path.isfile(cachedFileH):
            return False

        cachedFileSize = os.path.getsize(cachedFileH)
        currentFileSize = os.path.getsize(fileH)

        if cachedFileSize != currentFileSize:
            return False
        else:
            return True
    # end is_updated_cache_file

    def has_shebang(self):
        """Tests if the current file being queried contains a shebang line.

        The way it tests for a shebang line is to get the shebang snippet by-name,
        read whatever contents it contains, and then searches the top of the file
        for those lines.

        .. note::

            This operation is run when UltiSnips gets its snippet definitions.
            Since this happens extremely often, the operation is optimized
            by pickling the data in advance and re-querying when necessary.

            Times when it's re-queried:
                - If the snippets file changes
                - The pickled file definition is empty/invalid

        """
        shebangSnippet = find_snippet_by_name(snippets, "she")
        # remove the last two lines, since they're just a blank line and a tabstop
        for line in shebangSnippet.split('\n')[:-2]:
            # :RESUME:
            pass
    # end has_shebang

    def copy_snippet_file(self, fileH):
        """Copies a given snippet file to the root cache directory.

        A snippet file name or absolute path can be provided but the
        destination path is handled privately by the class instance.

        .. note::

            The function expects that the snippet file exists in _snippet_dir.
            If the file is located somewhere else, the copy operation will fail.

        Args:
            fileH (str): The full path of the snippet file to check and copy

        Returns:
            bool: The success or failure of the copy

        """
        fileName = os.path.basename(fileH)
        expectedCacheFileH = os.path.join(self._cached_root_dir, fileName)
        if not self.is_updated_cache_file(fileH):
            try:
                shutil.copy2(fileH, expectedCacheFileH)
                return True
            except:
                pass

        return False  # if the file wasn't copied for any reason, return False
    # end copy_snippet_file_copy

    def make_cached_snippet_definition(self, snippets=[], forceRebuild=False):
        """Copies the snippet definition into a temp folder.

        In other functions, this cached file is compared to the current file
        in order to figure out if the current file has been changed.

        Args:
            snippets (list): All of the snippets to be pickled.
            forceRebuild (bool): Forces a new cache of the current definitions

        Returns:
            tuple: The path to the cache and a list of what was cached

        """
        if not os.path.isfile(self._pickled_snippet_defs) or forceRebuild:
            # rebuild the file
            snippets = helper.UltiSnipsManagerExtended.list_snippets_objects()
            if snippets != []:
                fileObject = open(self._pickled_snippet_defs, 'wb')
                pickle.dump(snippets, fileObject)
        else:
            # read an existing file
            fileObject = open(self._pickled_snippet_defs, 'r')
            snippets = pickle.load(fileObject)
        fileObject.close()

        return (self._pickled_snippet_defs, snippets)
    # end make_cached_snippet_definition
# end Document


def main():
    """Creates UltiSnips helper functions, automates simple stuff.

    - Adds a shebang line to the top of the file if one doesn't exist

    .. note::

        Does this by detecting the expanded text for snippet 'she'
        and finds its contents from the top of the current file.

        requires that g:ua_temp_folder point to a valid, existing folder

    - If snippets.pickle doesn't exist yet, make it
    - If snippets definition changes, force a new pickling

    """
    doc = Document()
    forceUpdate = False

    # if not doc.is_updated_cache_file():
    #     forceUpdate = True

    # doc.make_cached_snippet_definition(snippets, forceUpdate)
# end main

main()
