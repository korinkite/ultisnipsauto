#!/usr/bin/env python
# -*- coding: utf-8 -*-

# IMPORT STANDARD LIBRARIES
import ast

# IMPORT LOCAL LIBRARIES
from pysix import six


def crop_to_docstring(strlines, outputLineNums=False, includeMarks=False):
    '''Gets the first found docstring from the input code.


    .. NOTE::

        If the input contains the outer quotes ""/'', then the output will, too

    :TODO: Make an optional output for line nums, which will be necessary for
    Style Guide classes later on

    :TODO: Force the output of the cropped docstring to include its original ''

    Args:
        strlines (str or iter): The snippet of code that contains the docstring

    Returns:
        str : The full docstring
    '''
    try:
        iter(strlines)
        if not isinstance(strlines, six.string_types):
            raise TypeError
    except TypeError:
        strlines = '\n'.join(strlines)

    module = ast.parse(strlines)
    defs = [node for node in module.body
            if isinstance(node, (ast.FunctionDef, ast.ClassDef, ast.Module))]
    for funcDef in defs:
        return ast.get_docstring(funcDef)
# end crop_to_docstring


if __name__ == '__main__':
    print(__doc__)

