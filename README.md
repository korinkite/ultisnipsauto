# UltiSnips AutoMate
> Make UltiSnips write for you
> Requires: Vim with Python features enabled

## Modules Overview
 * tempio - creates a root dir and gives it back to Vim for later access
 * shebang - automates adding a shebang line into Python files
 * helper - subclasses UltiSnips classes and adds any functions to use the module

## What It Does
It sets up a temporary directory using Python and assigns its absolute path to
the global variable, "g:ua_tempfolder", which points to the folder path.

UltiSnips AutoMate will handle the creation/deletion of the temp directory.

Currently, the folder is destroyed when Vim is opened and closed.




- compare the top lines with the first few of the string
- if exists, do nothing
- if not, add it



- vim make temp folder on startup
- delete it on close

- I'll need a temp file for the python.snippets file
 - compare the filesize for the current python.snippets file to that one temp one
  - if they are different, force a new pickle cache and delete the previous